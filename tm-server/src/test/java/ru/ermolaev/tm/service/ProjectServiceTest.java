package ru.ermolaev.tm.service;

import org.junit.Assert;
import org.junit.Test;
import ru.ermolaev.tm.api.repository.IProjectRepository;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.entity.Project;
import ru.ermolaev.tm.exception.empty.EmptyDescriptionException;
import ru.ermolaev.tm.exception.empty.EmptyIdException;
import ru.ermolaev.tm.exception.empty.EmptyNameException;
import ru.ermolaev.tm.exception.empty.EmptyUserIdException;
import ru.ermolaev.tm.exception.incorrect.IncorrectIndexException;
import ru.ermolaev.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;

public class ProjectServiceTest {

    final private IProjectRepository projectRepository = new ProjectRepository();

    final private IProjectService projectService = new ProjectService(projectRepository);

    @Test
    public void addProjectTest() throws Exception {
        Assert.assertTrue(projectService.findAll().isEmpty());
        final Project project = new Project();
        projectService.addProject("100", project);
        Assert.assertEquals(1, projectService.findAll().size());
        projectService.addProject("100", null);
        Assert.assertEquals(1, projectService.findAll().size());
    }

    @Test
    public void createProjectTest() throws Exception {
        Assert.assertTrue(projectService.findAll().isEmpty());
        projectService.createProject("100", "project");
        final Project project = projectService.findProjectByName("100", "project");
        Assert.assertNotNull(project);
        Assert.assertEquals("100", project.getUserId());
        Assert.assertEquals("project", project.getName());
        Assert.assertEquals(1, projectService.findAll().size());
    }

    @Test
    public void createProjectWithDescriptionTest() throws Exception {
        Assert.assertTrue(projectService.findAll().isEmpty());
        projectService.createProject("100", "project", "first project");
        final Project project = projectService.findProjectByName("100", "project");
        Assert.assertNotNull(project);
        Assert.assertEquals("100", project.getUserId());
        Assert.assertEquals("project", project.getName());
        Assert.assertEquals("first project", project.getDescription());
        Assert.assertEquals(1, projectService.findAll().size());
    }

    @Test
    public void findAllProjectsTest() throws Exception {
        Assert.assertTrue(projectService.findAll().isEmpty());
        for (int i = 0; i < 10; i++) {
            projectService.addProject("100", new Project());
        }
        projectService.createProject("200","project1");
        Assert.assertEquals(11, projectService.findAll().size());
        Assert.assertEquals(10, projectService.findAllProjects("100").size());
    }

    @Test
    public void findProjectByIdTest() throws Exception {
        Assert.assertTrue(projectService.findAll().isEmpty());
        final Project project = new Project();
        projectService.addProject("100", project);
        final Project tempProject = projectService.findProjectById("100", project.getId());
        Assert.assertNotNull(tempProject);
        Assert.assertEquals(project.getId(), tempProject.getId());
    }

    @Test
    public void findProjectByIndexTest() throws Exception {
        Assert.assertTrue(projectService.findAll().isEmpty());
        final Project project = new Project();
        projectService.addProject("100", project);
        final Project tempProject = projectService.findProjectByIndex("100", 0);
        Assert.assertNotNull(tempProject);
        Assert.assertEquals(project.getId(), tempProject.getId());
    }

    @Test
    public void findProjectByNameTest() throws Exception {
        Assert.assertTrue(projectService.findAll().isEmpty());
        final Project project = new Project();
        project.setName("project1");
        projectService.addProject("100", project);
        final Project tempProject = projectService.findProjectByName("100", project.getName());
        Assert.assertNotNull(tempProject);
        Assert.assertEquals(project.getId(), tempProject.getId());
    }

    @Test
    public void findAllTest() throws Exception {
        Assert.assertTrue(projectService.findAll().isEmpty());
        for (int i = 0; i < 10; i++) {
            projectService.addProject("100", new Project());
        }
        Assert.assertEquals(10, projectService.findAll().size());
    }

    @Test
    public void updateProjectByIdTest() throws Exception {
        Assert.assertTrue(projectService.findAll().isEmpty());
        final Project project = new Project();
        project.setName("project1");
        project.setDescription("project1");
        projectService.addProject("100", project);
        projectService.updateProjectById("100", project.getId(), "newName", "newDesc");
        Assert.assertEquals(1, projectService.findAll().size());
        final Project tempProject = projectService.findProjectByName("100", "newName");
        Assert.assertNotNull(tempProject);
        Assert.assertEquals(project.getId(), tempProject.getId());
        Assert.assertEquals("100", tempProject.getUserId());
        Assert.assertEquals("newName", tempProject.getName());
        Assert.assertEquals("newDesc", tempProject.getDescription());
    }

    @Test
    public void updateProjectByIndexTest() throws Exception {
        Assert.assertTrue(projectService.findAll().isEmpty());
        final Project project = new Project();
        project.setName("project1");
        project.setDescription("project1");
        projectService.addProject("100", project);
        projectService.updateProjectByIndex("100", 0, "newName", "newDesc");
        Assert.assertEquals(1, projectService.findAll().size());
        final Project tempProject = projectService.findProjectByName("100", "newName");
        Assert.assertNotNull(tempProject);
        Assert.assertEquals(project.getId(), tempProject.getId());
        Assert.assertEquals("100", tempProject.getUserId());
        Assert.assertEquals("newName", tempProject.getName());
        Assert.assertEquals("newDesc", tempProject.getDescription());
    }

    @Test
    public void removeProjectByIdTest() throws Exception {
        Assert.assertTrue(projectService.findAll().isEmpty());
        final Project project = new Project();
        projectService.addProject("100", project);
        Assert.assertEquals(1, projectService.findAll().size());
        projectService.removeProjectById("100", project.getId());
        Assert.assertEquals(0, projectService.findAll().size());
    }

    @Test
    public void removeProjectByIndexTest() throws Exception {
        Assert.assertTrue(projectService.findAll().isEmpty());
        projectService.addProject("100", new Project());
        Assert.assertEquals(1, projectService.findAll().size());
        projectService.removeProjectByIndex("100", 0);
        Assert.assertEquals(0, projectService.findAll().size());
    }

    @Test
    public void removeProjectByNameTest() throws Exception {
        Assert.assertTrue(projectService.findAll().isEmpty());
        final Project project = new Project();
        project.setName("project1");
        projectService.addProject("100", project);
        Assert.assertEquals(1, projectService.findAll().size());
        projectService.removeProjectByName("100", project.getName());
        Assert.assertEquals(0, projectService.findAll().size());
    }

    @Test
    public void removeProjectTest() throws Exception {
        Assert.assertTrue(projectService.findAll().isEmpty());
        final Project project = new Project();
        projectService.addProject("100", project);
        Assert.assertEquals(1, projectService.findAll().size());
        projectService.removeProject("100", project);
        Assert.assertEquals(0, projectService.findAll().size());

    }

    @Test
    public void removeAllProjectsTest() throws Exception {
        Assert.assertTrue(projectService.findAll().isEmpty());
        for (int i = 0; i < 5; i++) {
            projectService.addProject("100", new Project());
        }
        for (int i = 0; i < 5; i++) {
            projectService.addProject("200", new Project());
        }
        Assert.assertEquals(10, projectService.findAll().size());
        projectService.removeAllProjects("100");
        Assert.assertEquals(5, projectService.findAll().size());

    }

    @Test
    public void loadCollectionTest() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        List<Project> projects = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            projects.add(new Project());
        }
        projectService.load(projects);
        Assert.assertEquals(5, projectService.findAll().size());
    }

    @Test
    public void loadVarargsTest() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        Project[] projects = new Project[5];
        for (int i = 0; i < 5; i++) {
            projects[i] = new Project();
        }
        projectService.load(projects);
        Assert.assertEquals(5, projectService.findAll().size());
    }

    @Test(expected = EmptyUserIdException.class)
    public void createProjectWithEmptyUserIdTest() throws Exception {
        projectService.createProject("", "project");
    }

    @Test(expected = EmptyUserIdException.class)
    public void createProjectWithNullUserIdTest() throws Exception {
        projectService.createProject(null, "project");
    }

    @Test(expected = EmptyNameException.class)
    public void createProjectWithEmptyNameTest() throws Exception {
        projectService.createProject("100", "");
    }

    @Test(expected = EmptyNameException.class)
    public void createProjectWithNullNameTest() throws Exception {
        projectService.createProject("100", null);
    }

    @Test(expected = EmptyDescriptionException.class)
    public void createProjectWithEmptyDescriptionTest() throws Exception {
        projectService.createProject("100", "project", "");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void createProjectWithNullDescriptionTest() throws Exception {
        projectService.createProject("100", "project", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void addProjectWithEmptyUserIdTest() throws Exception {
        projectService.addProject("", new Project());
    }

    @Test(expected = EmptyUserIdException.class)
    public void addProjectWithNullUserIdTest() throws Exception {
        projectService.addProject(null, new Project());
    }

    @Test(expected = EmptyUserIdException.class)
    public void findAllProjectsWithEmptyUserIdTest() throws Exception {
        projectService.findAllProjects("");
    }

    @Test(expected = EmptyUserIdException.class)
    public void findAllProjectsWithNullUserIdTest() throws Exception {
        projectService.findAllProjects(null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void findProjectByIdWithEmptyUserIdTest() throws Exception {
        projectService.findProjectById("", "project");
    }

    @Test(expected = EmptyUserIdException.class)
    public void findProjectByIdWithNullUserIdTest() throws Exception {
        projectService.findProjectById(null, "project");
    }

    @Test(expected = EmptyIdException.class)
    public void findProjectByIdWithEmptyIdTest() throws Exception {
        projectService.findProjectById("100", "");
    }

    @Test(expected = EmptyIdException.class)
    public void findProjectByIdWithNullIdTest() throws Exception {
        projectService.findProjectById("100", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void findProjectByIndexWithEmptyUserIdTest() throws Exception {
        projectService.findProjectByIndex("", 0);
    }

    @Test(expected = EmptyUserIdException.class)
    public void findProjectByIndexWithNullUserIdTest() throws Exception {
        projectService.findProjectByIndex(null, 0);
    }

    @Test(expected = IncorrectIndexException.class)
    public void findProjectByIndexWithNullIndexTest() throws Exception {
        projectService.findProjectByIndex("100", null);
    }

    @Test(expected = IncorrectIndexException.class)
    public void findProjectByIndexWithIncorrectIndexTest() throws Exception {
        projectService.findProjectByIndex("100", -5);
    }

    @Test(expected = EmptyUserIdException.class)
    public void findProjectByNameWithEmptyUserIdTest() throws Exception {
        projectService.findProjectByName("", "project");
    }

    @Test(expected = EmptyUserIdException.class)
    public void findProjectByNameWithNullUserIdTest() throws Exception {
        projectService.findProjectByName(null, "project");
    }

    @Test(expected = EmptyNameException.class)
    public void findProjectByNameWithEmptyNameTest() throws Exception {
        projectService.findProjectByName("100", "");
    }

    @Test(expected = EmptyNameException.class)
    public void findProjectByNameWithNullNameTest() throws Exception {
        projectService.findProjectByName("100", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void updateProjectByIdWithEmptyUserIdTest() throws Exception {
        projectService.updateProjectById("", "100", "newName", "newDesc");
    }

    @Test(expected = EmptyUserIdException.class)
    public void updateProjectByIdWithNullUserIdTest() throws Exception {
        projectService.updateProjectById(null, "100", "newName", "newDesc");
    }

    @Test(expected = EmptyIdException.class)
    public void updateProjectByIdWithEmptyIdTest() throws Exception {
        projectService.updateProjectById("100", "", "newName", "newDesc");
    }

    @Test(expected = EmptyIdException.class)
    public void updateProjectByIdWithNullIdTest() throws Exception {
        projectService.updateProjectById("100", null, "newName", "newDesc");
    }

    @Test(expected = EmptyNameException.class)
    public void updateProjectByIdWithEmptyNameTest() throws Exception {
        projectService.updateProjectById("100", "100", "", "newDesc");
    }

    @Test(expected = EmptyNameException.class)
    public void updateProjectByIdWithNullNameTest() throws Exception {
        projectService.updateProjectById("100", "100", null, "newDesc");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void updateProjectByIdWithEmptyDescriptionTest() throws Exception {
        projectService.updateProjectById("100", "100", "newName", "");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void updateProjectByIdWithNullDescriptionTest() throws Exception {
        projectService.updateProjectById("100", "100", "newName", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void updateProjectByIndexWithEmptyUserIdTest() throws Exception {
        projectService.updateProjectByIndex("", 1, "newName", "newDesc");
    }

    @Test(expected = EmptyUserIdException.class)
    public void updateProjectByIndexWithNullUserIdTest() throws Exception {
        projectService.updateProjectByIndex(null, 1, "newName", "newDesc");
    }

    @Test(expected = IncorrectIndexException.class)
    public void updateProjectByIndexWithIncorrectIndexTest() throws Exception {
        projectService.updateProjectByIndex("100", -1, "newName", "newDesc");
    }

    @Test(expected = IncorrectIndexException.class)
    public void updateProjectByIndexWithNullIndexTest() throws Exception {
        projectService.updateProjectByIndex("100", null, "newName", "newDesc");
    }

    @Test(expected = EmptyNameException.class)
    public void updateProjectByIndexWithEmptyNameTest() throws Exception {
        projectService.updateProjectByIndex("100", 1, "", "newDesc");
    }

    @Test(expected = EmptyNameException.class)
    public void updateProjectByIndexWithNullNameTest() throws Exception {
        projectService.updateProjectByIndex("100", 1, null, "newDesc");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void updateProjectByIndexWithEmptyDescriptionTest() throws Exception {
        projectService.updateProjectByIndex("100", 1, "newName", "");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void updateProjectByIndexWithNullDescriptionTest() throws Exception {
        projectService.updateProjectByIndex("100", 1, "newName", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void removeProjectByIdWithEmptyUserIdTest() throws Exception {
        projectService.removeProjectById("", "project");
    }

    @Test(expected = EmptyUserIdException.class)
    public void removeProjectByIdWithNullUserIdTest() throws Exception {
        projectService.removeProjectById(null, "project");
    }

    @Test(expected = EmptyIdException.class)
    public void removeProjectByIdWithEmptyIdTest() throws Exception {
        projectService.removeProjectById("100", "");
    }

    @Test(expected = EmptyIdException.class)
    public void removeProjectByIdWithNullIdTest() throws Exception {
        projectService.removeProjectById("100", null);
    }

    @Test(expected = EmptyUserIdException.class)
    public void removeProjectByIndexWithEmptyUserIdTest() throws Exception {
        projectService.removeProjectByIndex("", 0);
    }

    @Test(expected = EmptyUserIdException.class)
    public void removeProjectByIndexWithNullUserIdTest() throws Exception {
        projectService.removeProjectByIndex(null, 0);
    }

    @Test(expected = IncorrectIndexException.class)
    public void removeProjectByIndexWithNullIndexTest() throws Exception {
        projectService.removeProjectByIndex("100", null);
    }

    @Test(expected = IncorrectIndexException.class)
    public void removeProjectByIndexWithIncorrectIndexTest() throws Exception {
        projectService.removeProjectByIndex("100", -5);
    }

    @Test(expected = EmptyUserIdException.class)
    public void removeProjectByNameWithEmptyUserIdTest() throws Exception {
        projectService.removeProjectByName("", "project");
    }

    @Test(expected = EmptyUserIdException.class)
    public void removeProjectByNameWithNullUserIdTest() throws Exception {
        projectService.removeProjectByName(null, "project");
    }

    @Test(expected = EmptyNameException.class)
    public void removeProjectByNameWithEmptyNameTest() throws Exception {
        projectService.removeProjectByName("100", "");
    }

    @Test(expected = EmptyNameException.class)
    public void removeProjectByNameWithNullNameTest() throws Exception {
        projectService.removeProjectByName("100", null);
    }

}

package ru.ermolaev.tm.service;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.ermolaev.tm.api.repository.ISessionRepository;
import ru.ermolaev.tm.api.service.ISessionService;
import ru.ermolaev.tm.bootstrap.Bootstrap;
import ru.ermolaev.tm.entity.Session;
import ru.ermolaev.tm.entity.User;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.exception.user.AccessDeniedException;
import ru.ermolaev.tm.repository.SessionRepository;

import java.util.List;

public class SessionServiceTest {

    final private ISessionRepository sessionRepository = new SessionRepository();

    final private Bootstrap bootstrap = new Bootstrap();

    final private ISessionService sessionService = new SessionService(sessionRepository, bootstrap);

    @Before
    public void prepareData() throws Exception {
        bootstrap.getUserService().create("admin","admin","email@test.ru");
    }

    @After
    public void removeData() throws Exception {
        bootstrap.getUserService().removeByLogin("admin");
    }

    @Test
    public void checkDataAccessTest() throws Exception {
        Assert.assertFalse(sessionService.checkDataAccess(null,"admin"));
        Assert.assertFalse(sessionService.checkDataAccess("admin",null));
        Assert.assertFalse(sessionService.checkDataAccess("","admin"));
        Assert.assertFalse(sessionService.checkDataAccess("admin",""));
        Assert.assertFalse(sessionService.checkDataAccess("admin","wrongPass"));
        Assert.assertTrue(sessionService.checkDataAccess("admin","admin"));
    }

    @Test
    public void isValidTest() throws Exception {
        final Session session = sessionService.open("admin","admin");
        Assert.assertNotNull(session);
        Assert.assertTrue(sessionService.isValid(session));
        final Session newSession = new Session();
        Assert.assertFalse(sessionService.isValid(newSession));
    }

    @Test
    public void openTest() throws Exception {
        Assert.assertNull(sessionService.open("admin1","admin"));
        final Session session = sessionService.open("admin","admin");
        Assert.assertNotNull(session);
        Assert.assertEquals(session, sessionRepository.findById(session.getId()));
    }

    @Test
    public void signTest() {
        final Session session = new Session();
        Assert.assertNotNull(sessionService.sign(session));
        Assert.assertNotNull(session.getSignature());
    }

    @Test
    public void getUserTest() throws Exception {
        final User user = bootstrap.getUserService().findByLogin("admin");
        final Session session = sessionService.open("admin","admin");
        Assert.assertNotNull(session);
        Assert.assertEquals(sessionService.getUser(session).getId(), user.getId());
    }

    @Test
    public void getUserIdTest() throws Exception {
        final Session session = sessionService.open("admin","admin");
        Assert.assertNotNull(session);
        final String userId = sessionService.getUserId(session);
        Assert.assertNotNull(userId);
        Assert.assertEquals(sessionService.getUser(session).getId(), userId);
    }

    @Test
    public void getSessionListTest() throws Exception {
        for (int i = 0; i < 5; i++) {
            sessionService.open("admin", "admin");
        }
        final Session session = sessionService.open("admin","admin");
        final List<Session> sessions = sessionService.getSessionList(session);
        Assert.assertNotNull(sessions);
        Assert.assertEquals(6, sessions.size());
    }

    @Test
    public void closeTest() throws Exception {
        final Session session = sessionService.open("admin","admin");
        final List<Session> sessions = sessionService.getSessionList(session);
        Assert.assertEquals(1, sessions.size());
        sessionService.close(session);
        Assert.assertEquals(0, sessionService.findAll().size());
    }

    @Test
    public void closeAllTest() throws Exception {
        for (int i = 0; i < 5; i++) {
            sessionService.open("admin", "admin");
        }
        final Session session = sessionService.open("admin","admin");
        final List<Session> sessions = sessionService.getSessionList(session);
        Assert.assertNotNull(sessions);
        Assert.assertEquals(6, sessions.size());
        sessionService.closeAll(session);
        Assert.assertEquals(0, sessionService.findAll().size());
    }

    @Test
    public void signOutByLoginTest() throws Exception {
        final User user = bootstrap.getUserService().findByLogin("admin");
        sessionService.open(user.getLogin(),"admin");
        Assert.assertEquals(1, sessionService.findAll().size());
        sessionService.signOutByLogin(user.getLogin());
        Assert.assertEquals(0, sessionService.findAll().size());
    }

    @Test
    public void signOutByUserIdTest() throws Exception {
        final User user = bootstrap.getUserService().findByLogin("admin");
        sessionService.open(user.getLogin(),"admin");
        Assert.assertEquals(1, sessionService.findAll().size());
        sessionService.signOutByUserId(user.getId());
        Assert.assertEquals(0, sessionService.findAll().size());
    }

    @Test(expected = AccessDeniedException.class)
    public void validateWithNullSessionTest() throws Exception {
        sessionService.validate(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void validateWithNullSignatureTest() throws Exception {
        final Session session = sessionService.open("admin","admin");
        session.setSignature(null);
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void validateWithNullStartTimeTest() throws Exception {
        final Session session = sessionService.open("admin","admin");
        session.setStartTime(null);
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void validateWithNullUserIdTest() throws Exception {
        final Session session = sessionService.open("admin","admin");
        session.setUserId(null);
        sessionService.validate(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void validateWithRemovedUserTest() throws Exception {
        final Session session = sessionService.open("admin","admin");
        bootstrap.getUserService().removeByLogin("admin");
        sessionService.validate(session, Role.USER);
    }

    @Test(expected = AccessDeniedException.class)
    public void validateWithIncorrectRoleTest() throws Exception {
        final Session session = sessionService.open("admin","admin");
        sessionService.validate(session, Role.ADMIN);
    }

    @Test(expected = AccessDeniedException.class)
    public void signOutByLoginWithNullLoginTest() throws Exception {
        sessionService.signOutByLogin(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void signOutByLoginWithEmptyLoginTest() throws Exception {
        sessionService.signOutByLogin("");
    }

    @Test(expected = AccessDeniedException.class)
    public void signOutByLoginWithRemovedUserTest() throws Exception {
        bootstrap.getUserService().removeByLogin("admin");
        sessionService.signOutByLogin("admin");
    }

    @Test(expected = AccessDeniedException.class)
    public void signOutByLoginWithNullUserIdTest() throws Exception {
        sessionService.signOutByUserId(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void signOutByLoginWithEmptyUserIdTest() throws Exception {
        sessionService.signOutByUserId("");
    }

}

package ru.ermolaev.tm.service;

import org.junit.*;
import ru.ermolaev.tm.api.repository.IUserRepository;
import ru.ermolaev.tm.api.service.IUserService;
import ru.ermolaev.tm.entity.User;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.exception.empty.*;
import ru.ermolaev.tm.repository.UserRepository;
import ru.ermolaev.tm.util.HashUtil;

public class UserServiceTest {

    final private IUserRepository userRepository = new UserRepository();

    final private IUserService userService = new UserService(userRepository);

    final private User user = new User("test", "pass", "test.ru");

    @Before
    public void addUserToRepository() {
        userRepository.add(user);
    }

    @After
    public void removeUserFromRepository() {
        userRepository.remove(user);
    }

    @Test
    public void findByIdTest() throws Exception {
        final User testUser = userService.findById(user.getId());
        Assert.assertNotNull(testUser);
        Assert.assertEquals(user.getId(), testUser.getId());
        Assert.assertNull(userService.findById("100"));
    }

    @Test
    public void findByLoginTest() throws Exception {
        final User testUser = userService.findByLogin(user.getLogin());
        Assert.assertNotNull(testUser);
        Assert.assertEquals(user.getId(), testUser.getId());
        Assert.assertNull(userService.findByLogin("admin"));
    }

    @Test
    public void findByEmailTest() throws Exception {
        final User testUser = userService.findByEmail(user.getEmail());
        Assert.assertNotNull(testUser);
        Assert.assertEquals(user.getId(), testUser.getId());
        Assert.assertNull(userService.findByEmail("email"));
    }

    @Test
    public void removeUserTest() throws Exception {
        Assert.assertNotNull(userService.findById(user.getId()));
        Assert.assertEquals(1, userService.findAll().size());
        Assert.assertNull(userService.removeUser(null));
        Assert.assertEquals(user, userService.removeUser(user));
        Assert.assertNull(userService.findById(user.getId()));
        Assert.assertEquals(0, userService.findAll().size());
    }

    @Test
    public void removeByIdTest() throws Exception {
        Assert.assertNotNull(userService.findById(user.getId()));
        Assert.assertEquals(1, userService.findAll().size());
        Assert.assertNull(userService.removeById("100"));
        Assert.assertEquals(user, userService.removeById(user.getId()));
        Assert.assertNull(userService.findById(user.getId()));
        Assert.assertEquals(0, userService.findAll().size());
    }

    @Test
    public void removeByLoginTest() throws Exception {
        Assert.assertNotNull(userService.findById(user.getId()));
        Assert.assertEquals(1, userService.findAll().size());
        Assert.assertNull(userService.removeByLogin("100"));
        Assert.assertEquals(user, userService.removeByLogin(user.getLogin()));
        Assert.assertNull(userService.findById(user.getId()));
        Assert.assertEquals(0, userService.findAll().size());
    }

    @Test
    public void removeByEmailTest() throws Exception {
        Assert.assertNotNull(userService.findById(user.getId()));
        Assert.assertEquals(1, userService.findAll().size());
        Assert.assertNull(userService.removeByEmail("100"));
        Assert.assertEquals(user, userService.removeByEmail(user.getEmail()));
        Assert.assertNull(userService.findById(user.getId()));
        Assert.assertEquals(0, userService.findAll().size());
    }

    @Test
    public void createTest() throws Exception {
        Assert.assertEquals(1, userService.findAll().size());
        final User testUser = userService.create("root", "pass");
        Assert.assertNotNull(testUser);
        Assert.assertEquals(testUser.getLogin(), userService.findById(testUser.getId()).getLogin());
        Assert.assertEquals(testUser.getPasswordHash(), userService.findById(testUser.getId()).getPasswordHash());
        Assert.assertEquals(testUser.getRole(), userService.findById(testUser.getId()).getRole());
        Assert.assertEquals(testUser.getRole(), Role.USER);
        Assert.assertEquals(2, userService.findAll().size());
    }

    @Test
    public void createWithEmailTest() throws Exception {
        Assert.assertEquals(1, userService.findAll().size());
        final User testUser = userService.create("root", "root", "email@test.ru");
        Assert.assertNotNull(testUser);
        Assert.assertEquals(testUser.getLogin(), userService.findById(testUser.getId()).getLogin());
        Assert.assertEquals(testUser.getPasswordHash(), userService.findById(testUser.getId()).getPasswordHash());
        Assert.assertEquals(testUser.getEmail(), userService.findById(testUser.getId()).getEmail());
        Assert.assertEquals(testUser.getRole(), userService.findById(testUser.getId()).getRole());
        Assert.assertEquals(testUser.getRole(), Role.USER);
        Assert.assertEquals(2, userService.findAll().size());
    }

    @Test
    public void createWithRoleTest() throws Exception {
        Assert.assertEquals(1, userService.findAll().size());
        final User testUser = userService.create("root", "root", Role.ADMIN);
        Assert.assertNotNull(testUser);
        Assert.assertEquals(testUser.getLogin(), userService.findById(testUser.getId()).getLogin());
        Assert.assertEquals(testUser.getRole(), userService.findById(testUser.getId()).getRole());
        Assert.assertEquals(testUser.getRole(), Role.ADMIN);
        Assert.assertEquals(2, userService.findAll().size());
    }

    @Test
    public void updatePasswordTest() throws Exception {
        Assert.assertEquals(user.getPasswordHash(), userService.findById(user.getId()).getPasswordHash());
        Assert.assertNull(userService.updatePassword("id", "newPass"));
        Assert.assertNotNull(userService.updatePassword(user.getId(), "newPass"));
        Assert.assertEquals(user.getPasswordHash(), HashUtil.hidePassword("newPass"));
    }

    @Test
    public void updateUserFirstNameTest() throws Exception {
        Assert.assertEquals(user.getFirstName(), userService.findById(user.getId()).getFirstName());
        Assert.assertNull(userService.updateUserFirstName("id", "newName"));
        Assert.assertNotNull(userService.updateUserFirstName(user.getId(), "newName"));
        Assert.assertEquals("newName", user.getFirstName());
    }

    @Test
    public void updateUserMiddleNameTest() throws Exception {
        Assert.assertEquals(user.getMiddleName(), userService.findById(user.getId()).getMiddleName());
        Assert.assertNull(userService.updateUserMiddleName("id", "newName"));
        Assert.assertNotNull(userService.updateUserMiddleName(user.getId(), "newName"));
        Assert.assertEquals("newName", user.getMiddleName());
    }

    @Test
    public void updateUserLastNameTest() throws Exception {
        Assert.assertEquals(user.getLastName(), userService.findById(user.getId()).getLastName());
        Assert.assertNull(userService.updateUserLastName("id", "newName"));
        Assert.assertNotNull(userService.updateUserLastName(user.getId(), "newName"));
        Assert.assertEquals("newName", user.getLastName());
    }

    @Test
    public void updateUserEmailTest() throws Exception {
        Assert.assertEquals(user.getEmail(), userService.findById(user.getId()).getEmail());
        Assert.assertNull(userService.updateUserEmail("id", "newEmail"));
        Assert.assertNotNull(userService.updateUserEmail(user.getId(), "newEmail"));
        Assert.assertEquals("newEmail", user.getEmail());
    }

    @Test
    public void lockUserByLoginTest() throws Exception {
        Assert.assertNotNull(userService.findById(user.getId()));
        Assert.assertFalse(user.getLocked());
        Assert.assertNull(userService.lockUserByLogin("log"));
        Assert.assertNotNull(userService.lockUserByLogin(user.getLogin()));
        Assert.assertTrue(user.getLocked());
    }

    @Test
    public void unlockUserByLoginTest() throws Exception {
        Assert.assertNotNull(userService.lockUserByLogin(user.getLogin()));
        Assert.assertTrue(user.getLocked());
        Assert.assertNull(userService.unlockUserByLogin("log"));
        Assert.assertNotNull(userService.unlockUserByLogin(user.getLogin()));
        Assert.assertFalse(user.getLocked());
    }

    @Test(expected = EmptyIdException.class)
    public void findByIdWithNullIdTest() throws Exception {
        userService.findById(null);
    }

    @Test(expected = EmptyIdException.class)
    public void findByIdWithEmptyIdTest() throws Exception {
        userService.findById("");
    }

    @Test(expected = EmptyLoginException.class)
    public void findByLoginWithNullLoginTest() throws Exception {
        userService.findByLogin(null);
    }

    @Test(expected = EmptyLoginException.class)
    public void findByLoginWithEmptyLoginTest() throws Exception {
        userService.findByLogin("");
    }

    @Test(expected = EmptyEmailException.class)
    public void findByEmailWithNullEmailTest() throws Exception {
        userService.findByEmail(null);
    }

    @Test(expected = EmptyEmailException.class)
    public void findByEmailWithEmptyEmailTest() throws Exception {
        userService.findByEmail("");
    }

    @Test(expected = EmptyIdException.class)
    public void removeByIdWithNullIdTest() throws Exception {
        userService.removeById(null);
    }

    @Test(expected = EmptyIdException.class)
    public void removeByIdWithEmptyIdTest() throws Exception {
        userService.removeById("");
    }

    @Test(expected = EmptyLoginException.class)
    public void removeByLoginWithNullLoginTest() throws Exception {
        userService.removeByLogin(null);
    }

    @Test(expected = EmptyLoginException.class)
    public void removeByLoginWithEmptyLoginTest() throws Exception {
        userService.removeByLogin("");
    }

    @Test(expected = EmptyEmailException.class)
    public void removeByEmailWithNullEmailTest() throws Exception {
        userService.removeByEmail(null);
    }

    @Test(expected = EmptyEmailException.class)
    public void removeByEmailWithEmptyEmailTest() throws Exception {
        userService.removeByEmail("");
    }

    @Test(expected = EmptyLoginException.class)
    public void creatWithNullLoginTest() throws Exception {
        userService.create(null,"pass");
    }

    @Test(expected = EmptyLoginException.class)
    public void creatWithEmptyLoginTest() throws Exception {
        userService.create("","pass");
    }

    @Test(expected = EmptyPasswordException.class)
    public void creatWithNullPasswordTest() throws Exception {
        userService.create("test",null);
    }

    @Test(expected = EmptyPasswordException.class)
    public void creatWithEmptyPasswordTest() throws Exception {
        userService.create("test","");
    }

    @Test(expected = EmptyEmailException.class)
    public void creatWithNullEmailTest() throws Exception {
        String pass = null;
        userService.create("test","pass", pass);
    }

    @Test(expected = EmptyEmailException.class)
    public void creatWithEmptyEmailTest() throws Exception {
        userService.create("test","pass", "");
    }

    @Test(expected = EmptyRoleException.class)
    public void creatWithNullRoleTest() throws Exception {
        Role role = null;
        userService.create("test","pass", role);
    }

    @Test(expected = EmptyLoginException.class)
    public void updatePasswordWithNullLoginTest() throws Exception {
        userService.updatePassword(null,"pass");
    }

    @Test(expected = EmptyLoginException.class)
    public void updatePasswordWithEmptyLoginTest() throws Exception {
        userService.updatePassword("","pass");
    }

    @Test(expected = EmptyPasswordException.class)
    public void updatePasswordWithNullPasswordTest() throws Exception {
        userService.updatePassword("test",null);
    }

    @Test(expected = EmptyPasswordException.class)
    public void updatePasswordWithEmptyPasswordTest() throws Exception {
        userService.updatePassword("test","");
    }

    @Test(expected = EmptyLoginException.class)
    public void updateUserFirstNameWithNullUserIdTest() throws Exception {
        userService.updateUserFirstName(null,"pass");
    }

    @Test(expected = EmptyLoginException.class)
    public void updateUserFirstNameWithEmptyUserIdTest() throws Exception {
        userService.updateUserFirstName("","pass");
    }

    @Test(expected = EmptyFirstNameException.class)
    public void updateUserFirstNameWithNullNameTest() throws Exception {
        userService.updateUserFirstName("test",null);
    }

    @Test(expected = EmptyFirstNameException.class)
    public void updateUserFirstNameWithEmptyNameTest() throws Exception {
        userService.updateUserFirstName("test","");
    }

    @Test(expected = EmptyLoginException.class)
    public void updateUserMiddleNameWithNullUserIdTest() throws Exception {
        userService.updateUserMiddleName(null,"pass");
    }

    @Test(expected = EmptyLoginException.class)
    public void updateUserMiddleNameWithEmptyUserIdTest() throws Exception {
        userService.updateUserMiddleName("","pass");
    }

    @Test(expected = EmptyMiddleNameException.class)
    public void updateUserMiddleNameWithNullNameTest() throws Exception {
        userService.updateUserMiddleName("test",null);
    }

    @Test(expected = EmptyMiddleNameException.class)
    public void updateUserMiddleNameWithEmptyNameTest() throws Exception {
        userService.updateUserMiddleName("test","");
    }

    @Test(expected = EmptyLoginException.class)
    public void updateUserLastNameWithNullUserIdTest() throws Exception {
        userService.updateUserLastName(null,"pass");
    }

    @Test(expected = EmptyLoginException.class)
    public void updateUserLastNameWithEmptyUserIdTest() throws Exception {
        userService.updateUserLastName("","pass");
    }

    @Test(expected = EmptyLastNameException.class)
    public void updateUserLastNameWithNullNameTest() throws Exception {
        userService.updateUserLastName("test",null);
    }

    @Test(expected = EmptyLastNameException.class)
    public void updateUserLastNameWithEmptyNameTest() throws Exception {
        userService.updateUserLastName("test","");
    }

    @Test(expected = EmptyLoginException.class)
    public void updateUserEmailWithNullUserIdTest() throws Exception {
        userService.updateUserEmail(null,"pass");
    }

    @Test(expected = EmptyLoginException.class)
    public void updateUserEmailWithEmptyUserIdTest() throws Exception {
        userService.updateUserEmail("","pass");
    }

    @Test(expected = EmptyEmailException.class)
    public void updateUserEmailWithNullEmailTest() throws Exception {
        userService.updateUserEmail("test",null);
    }

    @Test(expected = EmptyEmailException.class)
    public void updateUserEmailWithEmptyEmailTest() throws Exception {
        userService.updateUserEmail("test","");
    }

    @Test(expected = EmptyLoginException.class)
    public void lockUserByLoginWithNullUserIdTest() throws Exception {
        userService.lockUserByLogin(null);
    }

    @Test(expected = EmptyLoginException.class)
    public void lockUserByLoginWithEmptyUserIdTest() throws Exception {
        userService.lockUserByLogin("");
    }

    @Test(expected = EmptyLoginException.class)
    public void unlockUserByLoginWithNullUserIdTest() throws Exception {
        userService.unlockUserByLogin(null);
    }

    @Test(expected = EmptyLoginException.class)
    public void unlockUserByLoginWithEmptyUserIdTest() throws Exception {
        userService.unlockUserByLogin("");
    }

}

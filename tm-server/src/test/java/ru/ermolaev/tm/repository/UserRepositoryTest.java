package ru.ermolaev.tm.repository;

import org.junit.*;
import ru.ermolaev.tm.api.repository.IUserRepository;
import ru.ermolaev.tm.entity.User;

import java.util.Arrays;
import java.util.List;

public class UserRepositoryTest {

    final private IUserRepository userRepository = new UserRepository();

    final private static User userOne = new User();

    final private static User userTwo = new User();

    final private static User userThree = new User();

    final private static User[] userArray = {userOne, userTwo, userThree};

    final private static List<User> userList = Arrays.asList(userArray);

    @BeforeClass
    public static void initData() {
        userOne.setLogin("test");
        userOne.setEmail("test@test.ru");

        userTwo.setLogin("admin");
        userTwo.setEmail("admin@admin.ru");

        userThree.setLogin("user");
        userThree.setEmail("user@user.ru");
    }

    @Before
    public void addUser() {
        userRepository.add(userOne);
    }

    @After
    public void deleteUser() {
        userRepository.clear();
    }

    @Test
    public void createEmptyUserRepositoryTest() {
        final IUserRepository userRepository = new UserRepository();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void createNotEmptyUserRepositoryTest() {
        final IUserRepository userRepository = new UserRepository();
        userRepository.add(new User());
        Assert.assertFalse(userRepository.findAll().isEmpty());
    }

    @Test
    public void findByIdTest() {
        final User user = userRepository.findById(userOne.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getId(), userOne.getId());
    }

    @Test
    public void findByLoginTest() {
        final User user = userRepository.findByLogin(userOne.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getLogin(), userOne.getLogin());
    }

    @Test
    public void findByEmailTest() {
        final User user = userRepository.findByEmail(userOne.getEmail());
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getEmail(), userOne.getEmail());
    }

    @Test
    public void findAllTest() {
        userRepository.add(userTwo);
        final List<User> users = userRepository.findAll();
        Assert.assertNotNull(users);
        Assert.assertEquals(2,users.size());
    }

    @Test
    public void removeUserTest() {
        userRepository.remove(userOne);
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void removeByIdTest() {
        userRepository.removeById(userOne.getId());
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void removeByLoginTest() {
        userRepository.removeByLogin(userOne.getLogin());
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void removeByEmailTest() {
        userRepository.removeByEmail(userOne.getEmail());
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void addTest() {
        final User user =  userRepository.findById(userOne.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getId(), userOne.getId());
    }

    @Test
    public void addCollectionTest() {
        userRepository.add(userList);
        final List<User> users = userRepository.findAll();
        Assert.assertNotNull(users);
        Assert.assertEquals(4,users.size());
    }

    @Test
    public void addVarargsTest() {
        userRepository.add(userArray);
        final List<User> users = userRepository.findAll();
        Assert.assertNotNull(users);
        Assert.assertEquals(4,users.size());
    }

    @Test
    public void clearTest() {
        userRepository.add(userList);
        final List<User> users = userRepository.findAll();
        Assert.assertNotNull(users);
        Assert.assertEquals(4,users.size());
        userRepository.clear();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void loadCollectionTest() {
        userRepository.load(userList);
        final List<User> users = userRepository.findAll();
        Assert.assertNotNull(users);
        Assert.assertEquals(3,users.size());
    }

    @Test
    public void loadVarargsTest() {
        userRepository.load(userArray);
        final List<User> users = userRepository.findAll();
        Assert.assertNotNull(users);
        Assert.assertEquals(3,users.size());
    }

}

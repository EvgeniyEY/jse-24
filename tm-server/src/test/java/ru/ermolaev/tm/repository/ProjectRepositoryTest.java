package ru.ermolaev.tm.repository;

import org.junit.*;
import ru.ermolaev.tm.api.repository.IProjectRepository;
import ru.ermolaev.tm.entity.Project;
import ru.ermolaev.tm.exception.unknown.UnknownIdException;
import ru.ermolaev.tm.exception.unknown.UnknownIndexException;
import ru.ermolaev.tm.exception.unknown.UnknownNameException;

import java.util.Arrays;
import java.util.List;

public class ProjectRepositoryTest {

    final private IProjectRepository projectRepository = new ProjectRepository();

    final private static Project projectOne = new Project();

    final private static Project projectTwo = new Project();

    final private static Project projectThree = new Project();

    final private static Project[] projectArray = {projectOne, projectTwo, projectThree};

    final private static List<Project> projectList = Arrays.asList(projectArray);

    @BeforeClass
    public static void initData() {
        projectOne.setName("project-1");
        projectOne.setUserId("123123123");

        projectTwo.setName("project-2");
        projectTwo.setUserId("123123123");

        projectThree.setName("project-3");
        projectThree.setUserId("789789789789");
    }

    @Before
    public void addProject() {
        projectRepository.add(projectOne);
    }

    @After
    public void deleteProject() {
        projectRepository.clear();
    }

    @Test
    public void findByIdTest() throws Exception {
        final Project project = projectRepository.findById(projectOne.getUserId(), projectOne.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getName(), projectOne.getName());
    }

    @Test
    public void findByIndexTest() throws Exception {
        final Project project = projectRepository.findByIndex(projectOne.getUserId(),0);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getName(), projectOne.getName());
    }

    @Test
    public void findByNameTest() throws Exception {
        final Project project = projectRepository.findByName(projectOne.getUserId(), projectOne.getName());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getName(), projectOne.getName());
    }

    @Test
    public void findAllTest() {
        projectRepository.add(projectList);
        final List<Project> projects = projectRepository.findAllProjects(projectOne.getUserId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(projects.size(), 3);
    }

    @Test
    public void removeByIdTest() throws Exception {
        Assert.assertEquals(1, projectRepository.findAllProjects(projectOne.getUserId()).size());
        projectRepository.removeById(projectOne.getUserId(), projectOne.getId());
        Assert.assertEquals(0, projectRepository.findAllProjects(projectOne.getUserId()).size());
    }

    @Test
    public void removeByIndexTest() throws Exception {
        Assert.assertEquals(1, projectRepository.findAllProjects(projectOne.getUserId()).size());
        projectRepository.removeByIndex(projectOne.getUserId(), 0);
        Assert.assertEquals(0, projectRepository.findAllProjects(projectOne.getUserId()).size());
    }

    @Test
    public void removeByNameTest() throws Exception {
        Assert.assertEquals(1, projectRepository.findAllProjects(projectOne.getUserId()).size());
        projectRepository.removeByName(projectOne.getUserId(), projectOne.getName());
        Assert.assertEquals(0, projectRepository.findAllProjects(projectOne.getUserId()).size());
    }

    @Test
    public void removeTest() {
        Assert.assertEquals(1, projectRepository.findAllProjects(projectOne.getUserId()).size());
        projectRepository.remove(projectOne);
        Assert.assertEquals(0, projectRepository.findAllProjects(projectOne.getUserId()).size());
    }

    @Test
    public void clearTest() {
        projectRepository.add(projectList);
        Assert.assertEquals(3, projectRepository.findAllProjects(projectOne.getUserId()).size());
        projectRepository.clear(projectOne.getUserId());
        Assert.assertEquals(0, projectRepository.findAllProjects(projectOne.getUserId()).size());
    }

    @Test
    public void addTest() throws Exception {
        projectRepository.add(projectThree);
        final Project project = projectRepository.findById(projectThree.getUserId(), projectThree.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getName(), projectThree.getName());
    }

    @Test
    public void loadCollectionTest() {
        Assert.assertEquals(1, projectRepository.findAll().size());
        projectRepository.load(projectList);
        Assert.assertEquals(3, projectRepository.findAll().size());
    }

    @Test
    public void loadVarargsTest() {
        Assert.assertEquals(1, projectRepository.findAll().size());
        projectRepository.load(projectArray);
        Assert.assertEquals(3, projectRepository.findAll().size());
    }

    @Test(expected = UnknownIdException.class)
    public void findByIdExceptionTest() throws Exception {
        final Project project = projectRepository.findById(projectOne.getUserId(), "qweq");
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getName(), projectOne.getName());
    }

    @Test(expected = UnknownIndexException.class)
    public void findByIndexExceptionTest() throws Exception {
        final Project project = projectRepository.findByIndex(projectOne.getUserId(),-7);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getName(), projectOne.getName());
    }

    @Test(expected = UnknownNameException.class)
    public void findByNameExceptionTest() throws Exception {
        final Project project = projectRepository.findByName(projectOne.getUserId(), "qqw");
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getName(), projectOne.getName());
    }

    @Test(expected = UnknownIdException.class)
    public void removeByIdExceptionTest() throws Exception {
        Assert.assertEquals(1, projectRepository.findAllProjects(projectOne.getUserId()).size());
        projectRepository.removeById(projectOne.getUserId(), "qwq");
        Assert.assertEquals(0, projectRepository.findAllProjects(projectOne.getUserId()).size());
    }

    @Test(expected = UnknownIndexException.class)
    public void removeByIndexExceptionTest() throws Exception {
        Assert.assertEquals(1, projectRepository.findAllProjects(projectOne.getUserId()).size());
        projectRepository.removeByIndex(projectOne.getUserId(), -54);
        Assert.assertEquals(0, projectRepository.findAllProjects(projectOne.getUserId()).size());
    }

    @Test(expected = UnknownNameException.class)
    public void removeByNameExceptionTest() throws Exception {
        Assert.assertEquals(1, projectRepository.findAllProjects(projectOne.getUserId()).size());
        projectRepository.removeByName(projectOne.getUserId(), "12wqe");
        Assert.assertEquals(0, projectRepository.findAllProjects(projectOne.getUserId()).size());
    }

}

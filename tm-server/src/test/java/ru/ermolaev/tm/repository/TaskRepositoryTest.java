package ru.ermolaev.tm.repository;

import org.junit.*;
import ru.ermolaev.tm.api.repository.ITaskRepository;
import ru.ermolaev.tm.entity.Task;
import ru.ermolaev.tm.exception.unknown.UnknownIdException;
import ru.ermolaev.tm.exception.unknown.UnknownIndexException;
import ru.ermolaev.tm.exception.unknown.UnknownNameException;

import java.util.Arrays;
import java.util.List;

public class TaskRepositoryTest {

    final private ITaskRepository taskRepository = new TaskRepository();

    final private static Task taskOne = new Task();

    final private static Task taskTwo = new Task();

    final private static Task taskThree = new Task();

    final private static Task[] taskArray = {taskOne, taskTwo, taskThree};

    final private static List<Task> taskList = Arrays.asList(taskArray);

    @BeforeClass
    public static void initData() {
        taskOne.setName("task-1");
        taskOne.setUserId("123123123");

        taskTwo.setName("task-2");
        taskTwo.setUserId("123123123");

        taskThree.setName("task-3");
        taskThree.setUserId("789789789789");
    }

    @Before
    public void addTask() {
        taskRepository.add(taskOne);
    }

    @After
    public void deleteTask() {
        taskRepository.clear();
    }

    @Test
    public void findByIdTest() throws Exception {
        final Task task = taskRepository.findById(taskOne.getUserId(), taskOne.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getName(), taskOne.getName());
    }

    @Test
    public void findByIndexTest() throws Exception {
        final Task task = taskRepository.findByIndex(taskOne.getUserId(),0);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getName(), taskOne.getName());
    }

    @Test
    public void findByNameTest() throws Exception {
        final Task task = taskRepository.findByName(taskOne.getUserId(), taskOne.getName());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getName(), taskOne.getName());
    }

    @Test
    public void findAllTest() {
        taskRepository.add(taskList);
        final List<Task> tasks = taskRepository.findAllTasks(taskOne.getUserId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(tasks.size(), 3);
    }

    @Test
    public void removeByIdTest() throws Exception {
        Assert.assertEquals(1, taskRepository.findAllTasks(taskOne.getUserId()).size());
        taskRepository.removeById(taskOne.getUserId(), taskOne.getId());
        Assert.assertEquals(0, taskRepository.findAllTasks(taskOne.getUserId()).size());
    }

    @Test
    public void removeByIndexTest() throws Exception {
        Assert.assertEquals(1, taskRepository.findAllTasks(taskOne.getUserId()).size());
        taskRepository.removeByIndex(taskOne.getUserId(), 0);
        Assert.assertEquals(0, taskRepository.findAllTasks(taskOne.getUserId()).size());
    }

    @Test
    public void removeByNameTest() throws Exception {
        Assert.assertEquals(1, taskRepository.findAllTasks(taskOne.getUserId()).size());
        taskRepository.removeByName(taskOne.getUserId(), taskOne.getName());
        Assert.assertEquals(0, taskRepository.findAllTasks(taskOne.getUserId()).size());
    }

    @Test
    public void removeTest() {
        Assert.assertEquals(1, taskRepository.findAllTasks(taskOne.getUserId()).size());
        taskRepository.remove(taskOne);
        Assert.assertEquals(0, taskRepository.findAllTasks(taskOne.getUserId()).size());
    }

    @Test
    public void clearTest() {
        taskRepository.add(taskList);
        Assert.assertEquals(3, taskRepository.findAllTasks(taskOne.getUserId()).size());
        taskRepository.clear(taskOne.getUserId());
        Assert.assertEquals(0, taskRepository.findAllTasks(taskOne.getUserId()).size());
    }

    @Test
    public void addTest() throws Exception {
        taskRepository.add(taskThree);
        final Task task = taskRepository.findById(taskThree.getUserId(), taskThree.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getName(), taskThree.getName());
    }

    @Test
    public void loadCollectionTest() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        taskRepository.load(taskList);
        Assert.assertEquals(3, taskRepository.findAll().size());
    }

    @Test
    public void loadVarargsTest() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        taskRepository.load(taskArray);
        Assert.assertEquals(3, taskRepository.findAll().size());
    }

    @Test(expected = UnknownIdException.class)
    public void findByIdExceptionTest() throws Exception {
        final Task task = taskRepository.findById(taskOne.getUserId(), "qweq");
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getName(), taskOne.getName());
    }

    @Test(expected = UnknownIndexException.class)
    public void findByIndexExceptionTest() throws Exception {
        final Task task = taskRepository.findByIndex(taskOne.getUserId(),-7);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getName(), taskOne.getName());
    }

    @Test(expected = UnknownNameException.class)
    public void findByNameExceptionTest() throws Exception {
        final Task task = taskRepository.findByName(taskOne.getUserId(), "qqw");
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getName(), taskOne.getName());
    }

    @Test(expected = UnknownIdException.class)
    public void removeByIdExceptionTest() throws Exception {
        Assert.assertEquals(1, taskRepository.findAllTasks(taskOne.getUserId()).size());
        taskRepository.removeById(taskOne.getUserId(), "qwq");
        Assert.assertEquals(0, taskRepository.findAllTasks(taskOne.getUserId()).size());
    }

    @Test(expected = UnknownIndexException.class)
    public void removeByIndexExceptionTest() throws Exception {
        Assert.assertEquals(1, taskRepository.findAllTasks(taskOne.getUserId()).size());
        taskRepository.removeByIndex(taskOne.getUserId(), -54);
        Assert.assertEquals(0, taskRepository.findAllTasks(taskOne.getUserId()).size());
    }

    @Test(expected = UnknownNameException.class)
    public void removeByNameExceptionTest() throws Exception {
        Assert.assertEquals(1, taskRepository.findAllTasks(taskOne.getUserId()).size());
        taskRepository.removeByName(taskOne.getUserId(), "12wqe");
        Assert.assertEquals(0, taskRepository.findAllTasks(taskOne.getUserId()).size());
    }

}

package ru.ermolaev.tm.bootstrap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.api.endpoint.*;
import ru.ermolaev.tm.api.repository.*;
import ru.ermolaev.tm.api.service.*;
import ru.ermolaev.tm.endpoint.*;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.repository.*;
import ru.ermolaev.tm.service.*;

import javax.xml.ws.Endpoint;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthenticationService authenticationService = new AuthenticationService(userService);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IDomainService domainService = new DomainService(taskService, projectService, userService);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository, this);

    @NotNull
    private final LoadService loadService = new LoadService(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    private final IAdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this);

    @NotNull
    private final IAdminDataEndpoint adminDataEndpoint = new AdminDataEndpoint(this);

    @NotNull
    private final String url = "http://" + propertyService.getServerHost() + ":" + propertyService.getServerPort();

    @NotNull
    private final String sessionEndpointUrl = url + "/SessionEndpoint?wsdl";

    @NotNull
    private final String taskEndpointUrl = url + "/TaskEndpoint?wsdl";

    @NotNull
    private final String projectEndpointUrl = url + "/ProjectEndpoint?wsdl";

    @NotNull
    private final String userEndpointUrl = url + "/UserEndpoint?wsdl";

    @NotNull
    private final String adminUserEndpointUrl = url + "/AdminEndpoint?wsdl";

    @NotNull
    private final String adminDataEndpointUrl = url + "/AdminDataEndpoint?wsdl";

    private void initEndpoint() {
        Endpoint.publish(sessionEndpointUrl, sessionEndpoint);
        Endpoint.publish(taskEndpointUrl, taskEndpoint);
        Endpoint.publish(projectEndpointUrl, projectEndpoint);
        Endpoint.publish(userEndpointUrl, userEndpoint);
        Endpoint.publish(adminUserEndpointUrl, adminUserEndpoint);
        Endpoint.publish(adminDataEndpointUrl, adminDataEndpoint);
    }

    private void printEndpoint() {
        System.out.println(sessionEndpointUrl);
        System.out.println(taskEndpointUrl);
        System.out.println(projectEndpointUrl);
        System.out.println(userEndpointUrl);
        System.out.println(adminUserEndpointUrl);
        System.out.println(adminDataEndpointUrl);
    }

    private void initUsers() throws Exception {
        userService.create("user", "user", "user@test.ru");
        userService.create("test", "test", "test@test.ru");
        userService.create("guest", "guest", "guest@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
        userService.create("root", "root", Role.ADMIN);
    }

    @SneakyThrows
    public void run() {
        System.out.println("Welcome to task manager");
        initUsers();
        initEndpoint();
        printEndpoint();
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IAuthenticationService getAuthenticationService() {
        return authenticationService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public IDomainService getDomainService() {
        return domainService;
    }

    @NotNull
    @Override
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    @NotNull
    @Override
    public ISessionService getSessionService() {
        return sessionService;
    }

    @NotNull
    @Override
    public LoadService getLoadService() {
        return loadService;
    }

}

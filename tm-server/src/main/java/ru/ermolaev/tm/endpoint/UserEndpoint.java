package ru.ermolaev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.api.endpoint.IUserEndpoint;
import ru.ermolaev.tm.api.service.ServiceLocator;
import ru.ermolaev.tm.entity.Session;
import ru.ermolaev.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class UserEndpoint implements IUserEndpoint {

    private ServiceLocator serviceLocator;

    public UserEndpoint() {
    }

    public UserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    @WebMethod
    public User updateUserPassword(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "newPassword", partName = "newPassword") @Nullable final String newPassword
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updatePassword(session.getUserId(), newPassword);
    }

    @Nullable
    @Override
    @WebMethod
    public User updateUserFirstName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "newFirstName", partName = "newFirstName") @Nullable final String newFirstName
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updateUserFirstName(session.getUserId(), newFirstName);
    }

    @Nullable
    @Override
    @WebMethod
    public User updateUserMiddleName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "newMiddleName", partName = "newMiddleName") @Nullable final String newMiddleName
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updateUserMiddleName(session.getUserId(), newMiddleName);
    }

    @Nullable
    @Override
    @WebMethod
    public User updateUserLastName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "newLastName", partName = "newLastName") @Nullable final String newLastName
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updateUserLastName(session.getUserId(), newLastName);
    }

    @Nullable
    @Override
    @WebMethod
    public User updateUserEmail(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "newEmail", partName = "newEmail") @Nullable final String newEmail
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updateUserEmail(session.getUserId(), newEmail);
    }

    @Nullable
    @Override
    @WebMethod
    public User showUserProfile(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getSessionService().getUser(session);
    }

}

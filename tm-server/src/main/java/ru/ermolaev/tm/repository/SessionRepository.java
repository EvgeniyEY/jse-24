package ru.ermolaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.api.repository.ISessionRepository;
import ru.ermolaev.tm.entity.Session;
import ru.ermolaev.tm.exception.unknown.UnknownIdException;

import java.util.ArrayList;
import java.util.List;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public void add(@NotNull final String userId, @NotNull final Session session) {
        session.setUserId(userId);
        entities.add(session);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final Session session) {
        if (!userId.equals(session.getUserId())) return;
        entities.remove(session);
    }

    @Override
    public void clear(@NotNull final String userId) {
        entities.removeAll(findAllSessions(userId));
    }

    @NotNull
    @Override
    public List<Session> findAllSessions(@NotNull final String userId) {
        @NotNull final List<Session> result = new ArrayList<>();
        for (@NotNull final Session session: entities) {
            if (userId.equals(session.getUserId())) result.add(session);
        }
        return result;
    }

    @NotNull
    @Override
    public Session findByUserId(@NotNull final String userId) throws Exception {
        for (@NotNull final Session session : entities) {
            if (userId.equals(session.getUserId())) return session;
        }
        throw new UnknownIdException();
    }

    @NotNull
    @Override
    public Session findById(@NotNull final String id) throws Exception {
        for (@NotNull final Session session : entities) {
            if (id.equals(session.getId())) return session;
        }
        throw new UnknownIdException();
    }

    @NotNull
    @Override
    public Session removeByUserId(@NotNull final String userId) throws Exception {
        @NotNull final Session session = findByUserId(userId);
        remove(userId, session);
        return session;
    }

    @Override
    public boolean contains(@NotNull final String id) throws Exception {
        @NotNull final Session session = findById(id);
        return findAll().contains(session);
    }

}

package ru.ermolaev.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.util.HashUtil;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class User extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1001L;

    @NotNull
    private String login = "";

    @NotNull
    private String passwordHash = "";

    @NotNull
    private String email = "";

    @NotNull
    private String firstName = "";

    @NotNull
    private String middleName = "";

    @NotNull
    private String lastName = "";

    @NotNull
    private Role role = Role.USER;

    @NotNull
    private Boolean locked = false;

    public User(@NotNull final String login, @NotNull final String password, @NotNull final String email) {
        this.login = login;
        this.passwordHash = HashUtil.hidePassword(password);
        this.email = email;
    }

    @Override
    public String toString() {
        return "LOGIN: " + login + '\n' +
                "E-MAIL: " + email + '\n' +
                "FIRST-NAME: " + firstName + '\n' +
                "MIDDLE-NAME: " + middleName + '\n' +
                "LAST-NAME: " + lastName + '\n' +
                "ROLE: " + role
                ;
    }

}

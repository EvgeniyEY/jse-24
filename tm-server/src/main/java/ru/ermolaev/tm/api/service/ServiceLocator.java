package ru.ermolaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.service.LoadService;

public interface ServiceLocator {

    @NotNull
    IUserService getUserService();

    @NotNull
    IAuthenticationService getAuthenticationService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    LoadService getLoadService();

}

package ru.ermolaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.entity.User;

public interface IAuthenticationService {

    @NotNull
    String getUserId() throws Exception;

    boolean isAuthenticated();

    void login(@Nullable String login, @Nullable String password) throws Exception;

    void logout();

    void registration(@Nullable String login, @Nullable String password, @Nullable String email) throws Exception;

    void updatePassword(@Nullable String newPassword) throws Exception;

    @Nullable
    User findCurrentUser() throws Exception;

    void updateUserFirstName(@Nullable String newFirstName) throws Exception;

    void updateUserMiddleName(@Nullable String newMiddleName) throws Exception;

    void updateUserLastName(@Nullable String newLastName) throws Exception;

    void updateUserEmail(@Nullable String newEmail) throws Exception;

    void checkRole(@Nullable Role[] roles) throws Exception;

}

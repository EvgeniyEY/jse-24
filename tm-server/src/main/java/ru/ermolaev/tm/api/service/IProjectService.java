package ru.ermolaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.entity.Project;

import java.util.List;

public interface IProjectService extends IService<Project> {

    void createProject(@Nullable String userId, @Nullable String name) throws Exception;

    void createProject(@Nullable String userId, @Nullable String name, String description) throws Exception;

    void addProject(@Nullable String userId, @Nullable Project project) throws Exception;

    @NotNull
    List<Project> findAllProjects(@Nullable String userId) throws Exception;

    void removeProject(@Nullable String userId, @Nullable Project project) throws Exception;

    void removeAllProjects(@Nullable String userId) throws Exception;

    @NotNull
    Project findProjectById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull
    Project findProjectByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    @NotNull
    Project findProjectByName(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    Project updateProjectById(@Nullable String userId, @Nullable String id, @Nullable String name, String description) throws Exception;

    @NotNull
    Project updateProjectByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, String description) throws Exception;

    @NotNull
    Project removeProjectById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull
    Project removeProjectByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    @NotNull
    Project removeProjectByName(@Nullable String userId, @Nullable String name) throws Exception;

}

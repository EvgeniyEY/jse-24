package ru.ermolaev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    void add(@NotNull String userId, @NotNull Project project);

    void remove(@NotNull String userId, @NotNull Project project);

    void clear(@NotNull String userId);

    @NotNull
    List<Project> findAllProjects(@NotNull String userId);

    @NotNull
    Project findById(@NotNull String userId, @NotNull String id) throws Exception;

    @NotNull
    Project findByIndex(@NotNull String userId, @NotNull Integer index) throws Exception;

    @NotNull
    Project findByName(@NotNull String userId, @NotNull String name) throws Exception;

    @NotNull
    Project removeById(@NotNull String userId, @NotNull String id) throws Exception;

    @NotNull
    Project removeByIndex(@NotNull String userId, @NotNull Integer index) throws Exception;

    @NotNull
    Project removeByName(@NotNull String userId, @NotNull String name) throws Exception;

}

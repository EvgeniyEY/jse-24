package ru.ermolaev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.entity.Session;
import ru.ermolaev.tm.entity.Project;

import java.util.List;

public interface IProjectEndpoint {

    void createProject(@Nullable Session session, @Nullable String name, @Nullable String description) throws Exception;

    void clearProjects(@Nullable Session session) throws Exception;

    @NotNull
    List<Project> showAllProjects(@Nullable Session session) throws Exception;

    @NotNull
    Project showProjectById(@Nullable Session session, @Nullable String id) throws Exception;

    @NotNull
    Project showProjectByName(@Nullable Session session, @Nullable String name) throws Exception;

    @NotNull
    Project updateProjectById(@Nullable Session session, @Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    @NotNull
    Project removeProjectById(@Nullable Session session, @Nullable String id) throws Exception;

    @NotNull
    Project removeProjectByName(@Nullable Session session, @Nullable String name) throws Exception;
    
}

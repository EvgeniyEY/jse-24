package ru.ermolaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.api.repository.ISessionRepository;
import ru.ermolaev.tm.api.service.IPropertyService;
import ru.ermolaev.tm.api.service.ISessionService;
import ru.ermolaev.tm.api.service.ServiceLocator;
import ru.ermolaev.tm.entity.Session;
import ru.ermolaev.tm.entity.User;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.exception.user.AccessDeniedException;
import ru.ermolaev.tm.util.HashUtil;
import ru.ermolaev.tm.util.SignatureUtil;

import java.util.List;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    private final ServiceLocator serviceLocator;

    private final ISessionRepository repository;

    public SessionService(
            final ISessionRepository sessionRepository,
            final ServiceLocator serviceLocator
    ) {
        super(sessionRepository);
        this.repository = sessionRepository;
        this.serviceLocator = serviceLocator;
    }

    @Override
    public boolean checkDataAccess(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return false;
        @Nullable final String passwordHash = HashUtil.hidePassword(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    public boolean isValid(@Nullable final Session session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void validate(@Nullable final Session session) throws Exception {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null) throw new AccessDeniedException();
        if (session.getStartTime() == null) throw new AccessDeniedException();
        if (session.getUserId() == null) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @Nullable final String signatureSource = session.getSignature();
        @Nullable final String signatureTarget = sign(temp).getSignature();
        final boolean checkSignature = signatureSource.equals(signatureTarget);
        if (!checkSignature) throw new AccessDeniedException();
        if (!repository.contains(session.getId())) throw new AccessDeniedException();
    }

    @Override
    public void validate(@Nullable final Session session, @Nullable final Role role) throws Exception {
        if (role == null) return;
        validate(session);
        @NotNull final String userId = session.getUserId();
        @Nullable final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Nullable
    @Override
    public Session open(@Nullable final String login, @Nullable final String password) throws Exception {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return null;
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setStartTime(System.currentTimeMillis());
        repository.add(session);
        return sign(session);
    }

    @Nullable
    @Override
    public Session sign(@Nullable final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        final IPropertyService propertyService = serviceLocator.getPropertyService();
        final String salt = propertyService.getSessionSalt();
        final Integer cycle = propertyService.getSessionCycle();
        final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        return session;
    }

    @Nullable
    @Override
    public User getUser(@Nullable final Session session) throws Exception {
        @Nullable final String userId = getUserId(session);
        return serviceLocator.getUserService().findById(userId);
    }

    @Nullable
    @Override
    public String getUserId(@Nullable final Session session) throws Exception {
        validate(session);
        return session.getUserId();
    }

    @NotNull
    @Override
    public List<Session> getSessionList(@Nullable final Session session) throws Exception {
        validate(session);
        return repository.findAllSessions(session.getUserId());
    }

    @Override
    public void close(@Nullable final Session session) throws Exception {
        validate(session);
        repository.removeByUserId(session.getUserId());
    }

    @Override
    public void closeAll(@Nullable final Session session) throws Exception {
        validate(session);
        repository.clear(session.getUserId());
    }

    @Override
    public void signOutByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new AccessDeniedException();
        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        @NotNull final String userId = user.getId();
        repository.removeByUserId(userId);
    }

    @Override
    public void signOutByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        repository.removeByUserId(userId);
    }

}

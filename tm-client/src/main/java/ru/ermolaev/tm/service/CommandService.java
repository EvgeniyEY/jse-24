package ru.ermolaev.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.api.ICommandRepository;
import ru.ermolaev.tm.api.ICommandService;
import ru.ermolaev.tm.command.AbstractCommand;

import java.util.List;

public final class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @NotNull
    @Override
    public List<AbstractCommand> getCommandList() {
        return commandRepository.getCommandList();
    }

}

package ru.ermolaev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.endpoint.Session;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "project-create";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Create a new project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER PROJECT NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getProjectEndpoint().createProject(session, name, description);
        System.out.println("[COMPLETE]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}

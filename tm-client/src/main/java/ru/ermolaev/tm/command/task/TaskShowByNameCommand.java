package ru.ermolaev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.endpoint.Session;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.endpoint.Task;
import ru.ermolaev.tm.util.TerminalUtil;

public final class TaskShowByNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "task-show-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by name.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER TASK NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
        @NotNull final Task task = serviceLocator.getTaskEndpoint().showTaskByName(session, name);
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[COMPLETE]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}

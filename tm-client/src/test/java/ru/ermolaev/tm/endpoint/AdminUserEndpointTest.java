package ru.ermolaev.tm.endpoint;

import com.sun.xml.internal.ws.fault.ServerSOAPFaultException;
import org.junit.*;
import ru.ermolaev.tm.bootstrap.Bootstrap;

public class AdminUserEndpointTest {

    private final Bootstrap bootstrap = new Bootstrap();

    private final String testLogin = "userForTest";

    private final String testPass = "testPass";

    private final String testEmail = "testuser@test.ru";

    @Before
    public void prepareToTest() throws Exception_Exception {
        final Session session = bootstrap.getSessionEndpoint().openSession("admin", "admin");
        bootstrap.getSessionService().setCurrentSession(session);
        bootstrap.getAdminUserEndpoint().createUserWithEmail(testLogin, testPass, testEmail);
    }

    @After
    public void clearAfterTest() throws Exception_Exception {
        final Session session = bootstrap.getSessionService().getCurrentSession();
        bootstrap.getAdminUserEndpoint().removeUserByLogin(session, testLogin);
        bootstrap.getSessionEndpoint().closeSession(session);
        bootstrap.getSessionService().clearCurrentSession();
    }

    @Test
    public void createUserWithEmailTest() throws Exception_Exception {
        final Session userSession = bootstrap.getSessionEndpoint().openSession(testLogin, testPass);
        Assert.assertNotNull(userSession);
        final User user = bootstrap.getUserEndpoint().showUserProfile(userSession);
        Assert.assertNotNull(user);
        Assert.assertEquals(testLogin, user.getLogin());
        Assert.assertEquals(testEmail, user.getEmail());
        bootstrap.getSessionEndpoint().closeSession(userSession);
    }

    @Test
    public void createUserWithRoleTest() throws Exception_Exception {
        final Session adminSession = bootstrap.getSessionService().getCurrentSession();
        Assert.assertNotNull(adminSession);
        final String testLoginRole = "logRole";
        bootstrap.getAdminUserEndpoint().createUserWithRole(adminSession, testLoginRole, testPass, Role.ADMIN);
        final Session userSession = bootstrap.getSessionEndpoint().openSession(testLoginRole, testPass);
        Assert.assertNotNull(userSession);
        final User user = bootstrap.getUserEndpoint().showUserProfile(userSession);
        Assert.assertNotNull(user);
        Assert.assertEquals(testLoginRole, user.getLogin());
        Assert.assertEquals(Role.ADMIN, user.getRole());
        bootstrap.getSessionEndpoint().closeSession(userSession);
        bootstrap.getAdminUserEndpoint().removeUserByLogin(adminSession, testLoginRole);
    }

    @Test
    public void lockUserByLoginTest() throws Exception_Exception {
        final Session adminSession = bootstrap.getSessionService().getCurrentSession();
        Assert.assertNotNull(adminSession);
        final Session userSession = bootstrap.getSessionEndpoint().openSession(testLogin, testPass);
        Assert.assertNotNull(userSession);
        User user = bootstrap.getUserEndpoint().showUserProfile(userSession);
        Assert.assertNotNull(user);
        Assert.assertFalse(user.isLocked());
        bootstrap.getAdminUserEndpoint().lockUserByLogin(adminSession,testLogin);
        user = bootstrap.getUserEndpoint().showUserProfile(userSession);
        Assert.assertNotNull(user);
        Assert.assertTrue(user.isLocked());
        bootstrap.getSessionEndpoint().closeSession(userSession);
    }

    @Test
    public void unlockUserByLoginTest() throws Exception_Exception {
        final Session adminSession = bootstrap.getSessionService().getCurrentSession();
        Assert.assertNotNull(adminSession);
        final Session userSession = bootstrap.getSessionEndpoint().openSession(testLogin, testPass);
        Assert.assertNotNull(userSession);
        bootstrap.getAdminUserEndpoint().lockUserByLogin(adminSession,testLogin);
        User user = bootstrap.getUserEndpoint().showUserProfile(userSession);
        Assert.assertNotNull(user);
        Assert.assertTrue(user.isLocked());
        bootstrap.getAdminUserEndpoint().unlockUserByLogin(adminSession,testLogin);
        user = bootstrap.getUserEndpoint().showUserProfile(userSession);
        Assert.assertNotNull(user);
        Assert.assertFalse(user.isLocked());
        bootstrap.getSessionEndpoint().closeSession(userSession);
    }

    @Test
    public void removeUserByLoginTest() throws Exception_Exception {
        final Session userSession = bootstrap.getSessionEndpoint().openSession(testLogin, testPass);
        Assert.assertNotNull(userSession);
        final User user = bootstrap.getUserEndpoint().showUserProfile(userSession);
        Assert.assertNotNull(user);
        final Session adminSession = bootstrap.getSessionService().getCurrentSession();
        Assert.assertNotNull(adminSession);
        bootstrap.getAdminUserEndpoint().removeUserByLogin(adminSession, testLogin);
        Assert.assertNull(bootstrap.getUserEndpoint().showUserProfile(userSession));
        bootstrap.getSessionEndpoint().closeSession(userSession);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void createUserWithRoleWithNullSessionTest() throws Exception_Exception {
        bootstrap.getAdminUserEndpoint().createUserWithRole(null, testLogin, testPass, Role.ADMIN);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void createUserWithRoleWithInvalidSessionTest() throws Exception_Exception {
        final Session fakeSession = new Session();
        bootstrap.getAdminUserEndpoint().createUserWithRole(fakeSession, testLogin, testPass, Role.ADMIN);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void createUserWithRoleByNotAdminTest() throws Exception_Exception {
        final Session userSession = bootstrap.getSessionEndpoint().openSession(testLogin, testPass);
        Assert.assertNotNull(userSession);
        bootstrap.getAdminUserEndpoint().createUserWithRole(userSession, "testLogin", "testPass", Role.ADMIN);
        bootstrap.getSessionEndpoint().closeSession(userSession);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void lockUserByLoginWithNullSessionTest() throws Exception_Exception {
        bootstrap.getAdminUserEndpoint().lockUserByLogin(null, testLogin);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void lockUserByLoginWithInvalidSessionTest() throws Exception_Exception {
        final Session fakeSession = new Session();
        bootstrap.getAdminUserEndpoint().lockUserByLogin(fakeSession, testLogin);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void lockUserByLoginByNotAdminTest() throws Exception_Exception {
        final Session userSession = bootstrap.getSessionEndpoint().openSession(testLogin, testPass);
        Assert.assertNotNull(userSession);
        bootstrap.getAdminUserEndpoint().lockUserByLogin(userSession, "admin");
        bootstrap.getSessionEndpoint().closeSession(userSession);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void unlockUserByLoginWithNullSessionTest() throws Exception_Exception {
        bootstrap.getAdminUserEndpoint().unlockUserByLogin(null, testLogin);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void unlockUserByLoginWithInvalidSessionTest() throws Exception_Exception {
        final Session fakeSession = new Session();
        bootstrap.getAdminUserEndpoint().unlockUserByLogin(fakeSession, testLogin);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void unlockUserByLoginByNotAdminTest() throws Exception_Exception {
        final Session userSession = bootstrap.getSessionEndpoint().openSession(testLogin, testPass);
        Assert.assertNotNull(userSession);
        bootstrap.getAdminUserEndpoint().unlockUserByLogin(userSession, "admin");
        bootstrap.getSessionEndpoint().closeSession(userSession);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void removeUserByLoginWithNullSessionTest() throws Exception_Exception {
        bootstrap.getAdminUserEndpoint().removeUserByLogin(null, testLogin);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void removeUserByLoginWithInvalidSessionTest() throws Exception_Exception {
        final Session fakeSession = new Session();
        bootstrap.getAdminUserEndpoint().removeUserByLogin(fakeSession, testLogin);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void removeUserByLoginByNotAdminTest() throws Exception_Exception {
        final Session userSession = bootstrap.getSessionEndpoint().openSession(testLogin, testPass);
        Assert.assertNotNull(userSession);
        bootstrap.getAdminUserEndpoint().removeUserByLogin(userSession, "admin");
        bootstrap.getSessionEndpoint().closeSession(userSession);
    }

}
